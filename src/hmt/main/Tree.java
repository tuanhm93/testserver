/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hmt.main;

import java.util.ArrayList;

/**
 *
 * @author tuanhm
 */
public class Tree {
    private Node root; //Nút gốc chính là nút chứa thông tin tổng giám đốc
    private ArrayList<Node> dsnv; //Dạnh sách nhân viên
    private ArrayList<Node> dspb; //Danh sách phòng ban

    public Tree(Node root, ArrayList<Node> dsnv, ArrayList<Node> dspb) {
        this.root = root;
        this.dsnv = dsnv;
        this.dspb = dspb;
    }
    //Hàm thêm nhân viên mới
    public void addNew(int idNV, String name, int idPB) {
        System.out.println("Tác vụ: Thêm nhân viên mới ");
        if (name.trim().equals("")) {
            System.out.println("***Tên không hợp lệ");
            return;
        }
        System.out.println("***Kiểm tra xem id nhân viên đã có hay chưa:");
        int indexNV = search(idNV, dsnv);
        if (indexNV == -1) {
            System.out.println("*****Id chưa trùng");
            System.out.println("*****Kiểm tra xem id phòng ban có đúng hay không");
            int indexPb = search(idPB, dspb);
            if (indexPb == -1) {
                System.out.println("********Không có id phòng ban nào như đầu vào => Thêm nhân viên thất bại");
            } else {
                System.out.println("********Tồn tại phòng ban có id như đầu vào: " + dspb.get(indexPb));
                System.out.println("********Tạo node mới trỏ node cha là phòng ban, thêm node vào danh sách node của phòng ban");
                Node node = new Node(idNV, name, 5, null, dspb.get(indexPb));
                dspb.get(indexPb).getChilds().add(node);
                System.out.println("********Thêm node vào danh sách nhân viên vị trí thích hợp đảm bảo id tăng dần");
                int size = dsnv.size();
                if (idNV < dsnv.get(0).getId()) {
                    dsnv.add(0, node);
                } else if (idNV > dsnv.get(size - 1).getId()) {
                    dsnv.add(node);
                } else {
                    for (int i = 0; i < size - 1; i++) {
                        if ((dsnv.get(i).getId() < idNV) && (idNV < dsnv.get(i + 1).getId())) {
                            dsnv.add(i + 1, node);
                            break;
                        }
                    }
                }

                System.out.println("");
                show();
            }
        } else {
            System.out.println("*****Id đã trùng " + dsnv.get(indexNV));
        }
    }
    //Hàm đổi chức vụ giữa 2 nhân viên có id1, id2
    public void doiChucVu(int id1, int id2) {
        System.out.println("Tác vụ: Đổi chức vụ");
        if (id1 == id2) {
            System.out.println("***Id trùng nhau");
            return;
        }
        System.out.println("Lấy thông tin hai người cần đổi chức vụ");
        int indexNV1 = search(id1, dsnv);
        int indexNV2 = search(id2, dsnv);
        if (indexNV1 == -1) {
            System.out.println("***Không tồn tại nhân viên có id:" + id1);
        } else if (indexNV2 == -1) {
            System.out.println("***Không tồn tại nhân viên có id:" + id2);
        } else {
            System.out.println("***" + dsnv.get(indexNV1));
            System.out.println("***" + dsnv.get(indexNV2));
            boolean b = false;
            if (((dsnv.get(indexNV1).getLevel() == 1) && (dsnv.get(indexNV2).getLevel() == 2))
                    || ((dsnv.get(indexNV1).getLevel() == 2) && (dsnv.get(indexNV2).getLevel() == 1))) {
                //Trường hợp 1 là tổng giám đốc, 1 là phó giám đốc
                b = true;
                System.out.println("***Do là tổng giám đốc và phó giám đốc nên đổi chức vụ thành công");
            } else if ((dsnv.get(indexNV1).getLevel() == 2) && (dsnv.get(indexNV2).getLevel() == 3)) {
                //Trường hợp 1 là phó giám đốc, 1 là trưởng phòng
                //Trưởng phòng đó phải thuộc phòng ban mà phó giám đốc đó quản lý
                if ((dsnv.get(indexNV2).getParentNode().getParentNode().getId() == id1)) {
                    System.out.println("***Do trưởng phòng thuộc phòng ban phó giám đốc quản lý nên đổi chức vụ thành công");
                    b = true;
                } else {
                    System.out.println("***Do trưởng phòng không thuộc phòng ban phó giám đốc quản lý nên đổi chức vụ không thành công");
                }

            } else if ((dsnv.get(indexNV1).getLevel() == 3) && (dsnv.get(indexNV2).getLevel() == 2)) {
                //Trường hợp 1 là phó giám đốc, 1 là trưởng phòng
                //Trưởng phòng đó phải thuộc phòng ban mà phó giám đốc đó quản lý
                if ((dsnv.get(indexNV1).getParentNode().getParentNode().getId() == id2)) {
                    System.out.println("***Do trưởng phòng thuộc phòng ban phó giám đốc quản lý nên đổi chức vụ thành công");
                    b = true;
                } else {
                    System.out.println("***Do trưởng phòng không thuộc phòng ban phó giám đốc quản lý nên đổi chức vụ không thành công");
                }
            } else if (((dsnv.get(indexNV1).getLevel() == 3) && (dsnv.get(indexNV2).getLevel() == 4))
                    || ((dsnv.get(indexNV1).getLevel() == 4) && (dsnv.get(indexNV2).getLevel() == 3))
                    || ((dsnv.get(indexNV1).getLevel() == 4) && (dsnv.get(indexNV2).getLevel() == 5))
                    || ((dsnv.get(indexNV1).getLevel() == 5) && (dsnv.get(indexNV2).getLevel() == 4))) {
                //Trường hợp 1 là trưởng phòng và 1 là phó phòng hoặc 1 là phó phòng và 1 là nhân viên
                //Phải thuộc cùng phòng ban
                if (dsnv.get(indexNV1).getParentNode().getId() == dsnv.get(indexNV2).getParentNode().getId()) {
                    System.out.println("***Do cùng thuộc phòng ban nên đổi chức vụ thành công");
                    b = true;
                } else {
                    System.out.println("***Do không thuộc cùng phòng ban nên đổi chức vụ không thành công");
                }
            }else{
                System.out.println("***Không thể nhảy chức hoặc không cần thiết đổi chức");
            }
            if (b) {
                String nameNV1 = dsnv.get(indexNV1).getName();
                dsnv.get(indexNV1).setName(dsnv.get(indexNV2).getName());
                dsnv.get(indexNV1).setId(id2);
                dsnv.get(indexNV2).setName(nameNV1);
                dsnv.get(indexNV2).setId(id1);
                Node nv1 = dsnv.get(indexNV1);
                dsnv.remove(indexNV1);
                dsnv.add(indexNV1, dsnv.get(indexNV2));
                dsnv.remove(indexNV2);
                dsnv.add(indexNV2, nv1);
                System.out.println("");
                show();
            }
        }
    }
    //Hàm tìm kiếm id trong array trả về index nếu tìm thấy, -1 nếu k tìm thấy
    public static int search(int id, ArrayList<Node> array) {
        int index = -1;
        int left = 0;
        int right = array.size() - 1;
        int mid;
        while (left <= right) {
            mid = (left + right) / 2;
            if (array.get(mid).getId() == id) {
                index = mid;
                break;
            } else if (array.get(mid).getId() < id) {
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return index;
    }
    //Hiển thị cấu trúc công ty
    public void show() {
        if (root == null) {
            return;
        }
        System.out.println(root);
        ArrayList<Node> pgdList = root.getChilds();
        if (pgdList != null) {
            int sizePgdList = pgdList.size();
            for (int i = 0; i < sizePgdList; i++) {
                System.out.println("***** " + pgdList.get(i));
                ArrayList<Node> pbList = pgdList.get(i).getChilds();
                if (pbList != null) {
                    int sizePbList = pbList.size();
                    for (int j = 0; j < sizePbList; j++) {
                        System.out.println("********** " + pbList.get(j));
                        ArrayList<Node> dsnvList = pbList.get(j).getChilds();
                        if (dsnvList != null) {
                            int sizeDsnvList = dsnvList.size();
                            for (int k = 0; k < sizeDsnvList; k++) {
                                System.out.println("*************** " + dsnvList.get(k));
                            }
                        }
                    }
                }
            }
        }
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public ArrayList<Node> getDsnv() {
        return dsnv;
    }

    public void setDsnv(ArrayList<Node> dsnv) {
        this.dsnv = dsnv;
    }

    public ArrayList<Node> getDspb() {
        return dspb;
    }

    public void setDspb(ArrayList<Node> dspb) {
        this.dspb = dspb;
    }

}
