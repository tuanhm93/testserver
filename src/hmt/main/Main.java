/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hmt.main;

import java.util.ArrayList;

/**
 *
 * @author tuanhm
 */
public class Main {
    public static void main(String[] args) {
        Node tgd = new Node(1, "Tổng giám đốc", 1, null, null);
        
        Node pgd1 = new Node(2, "Phó giám đốc 1", 2, null, null);
        Node pgd2 = new Node(3, "Phó giám đốc 2", 2, null, null);
        
        Node tp1 = new Node(4, "Trưởng phòng 1", 3, null, null);
        Node tp2 = new Node(8, "Trưởng phòng 2", 3, null, null);
        Node tp3 = new Node(11, "Trưởng phòng 3", 3, null, null);
        
        Node pp1 = new Node(5, "Phó phòng 1", 4, null, null);
        Node pp2 = new Node(9, "Phó phòng 2", 4, null, null);
        Node pp3 = new Node(12, "Phó phòng 3", 4, null, null);
        
        Node nv11 = new Node(6, "Nhân viên 11", 5, null, null);
        Node nv12 = new Node(7, "Nhân viên 12", 5, null, null);
        Node nv21 = new Node(10, "Nhân viên 21", 5, null, null);
        Node nv31 = new Node(13, "Nhân viên 31", 5, null, null);
        
        Node pb1 = new Node(1, "Phòng ban 1", 0, null, null);
        Node pb2 = new Node(2, "Phòng ban 2", 0, null, null);
        Node pb3 = new Node(3, "Phòng ban 3", 0, null, null);
        
        ArrayList<Node> dsnvp1 = new ArrayList<Node>();
        dsnvp1.add(tp1);
        dsnvp1.add(pp1);
        dsnvp1.add(nv11);
        dsnvp1.add(nv12);
        
        pb1.setChilds(dsnvp1);
        tp1.setParentNode(pb1);
        pp1.setParentNode(pb1);
        nv11.setParentNode(pb1);
        nv12.setParentNode(pb1);
        
        ArrayList<Node> dsnvp2 = new ArrayList<Node>();
        dsnvp2.add(tp2);
        dsnvp2.add(pp2);
        dsnvp2.add(nv21);
        
        pb2.setChilds(dsnvp2);
        tp2.setParentNode(pb2);
        pp2.setParentNode(pb2);
        nv21.setParentNode(pb2);
        
        ArrayList<Node> dsnvp3 = new ArrayList<Node>();
        dsnvp3.add(tp3);
        dsnvp3.add(pp3);
        dsnvp3.add(nv31);
        
        pb3.setChilds(dsnvp3);
        tp3.setParentNode(pb3);
        pp3.setParentNode(pb3);
        nv31.setParentNode(pb3);
        
        ArrayList<Node> dspbGd1 = new ArrayList<Node>();
        dspbGd1.add(pb1);
        dspbGd1.add(pb2);
        
        pgd1.setChilds(dspbGd1);
        pb1.setParentNode(pgd1);
        pb2.setParentNode(pgd1);
        
        ArrayList<Node> dspbGd2 = new ArrayList<Node>();
        dspbGd2.add(pb3);
        
        pgd2.setChilds(dspbGd2);
        pb3.setParentNode(pgd2);
 
        ArrayList<Node> dspgd = new ArrayList<Node>();
        dspgd.add(pgd1);
        dspgd.add(pgd2);
        
        tgd.setChilds(dspgd);
        pgd1.setParentNode(tgd);
        pgd2.setParentNode(tgd);
        
        ArrayList<Node> dsnv = new ArrayList<Node>();
        dsnv.add(tgd);
        dsnv.add(pgd1);
        dsnv.add(pgd2);
        dsnv.add(tp1);
        dsnv.add(pp1);
        dsnv.add(nv11);
        dsnv.add(nv12);
        dsnv.add(tp2);
        dsnv.add(pp2);
        dsnv.add(nv21);
        dsnv.add(tp3);
        dsnv.add(pp3);
        dsnv.add(nv31);
        
        ArrayList<Node> dspb = new ArrayList<Node>();
        dspb.add(pb1);
        dspb.add(pb2);
        dspb.add(pb3);
        
        Tree tree = new Tree(tgd, dsnv, dspb);
        tree.show();
        System.out.println("----------------------------------------------");
        
//        tree.addNew(1, "Hoàng Minh Tuấn", 3); //Thêm id trùng
//        System.out.println("");
//        tree.addNew(14, "Hoàng Minh Tuấn", 4); //Thêm id phòng ban không tồn tại
//        System.out.println("");
        tree.addNew(14, "Hoàng Minh Tuấn", 1); // Thêm nhân viên có id 14 vào phòng ban 1
        
        tree.doiChucVu(1, 2);//Đổi chức vụ của nhân viên có id = 1 và nhân viên có  id = 2
        
        
    }
}
