/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hmt.main;

import java.util.ArrayList;

/**
 *
 * @author tuanhm
 */
public class Node {
    private int id; // id xác định duy nhất người hoặc phòng ban
    private String name; // tên người hoặc phòng ban
    private int level; // Dùng để phân biệt
    private ArrayList<Node> childs; // Danh sách các nút con
    private Node parentNode; // trỏ tới node cha


    public Node(int id, String name, int level, ArrayList<Node> childs, Node parentNode) {
        this.level = level;
        this.id = id;
        this.name = name;
        this.childs = childs;
        this.parentNode = parentNode;
    }
    
    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Node> getChilds() {
        return childs;
    }

    public void setChilds(ArrayList<Node> childs) {
        this.childs = childs;
    }

    public Node getParentNode() {
        return parentNode;
    }

    public void setParentNode(Node parentNode) {
        this.parentNode = parentNode;
    }
    
    public String toString(){
        String str;
        if(level == 0){
            str = "Phòng: ";
        }else if(level == 1){
            str = "Tổng giám đốc: ";
        }else if(level == 2){
            str = "Phó giám đốc: ";
        }else if(level == 3){
            str = "Trưởng phòng: ";
        }else if(level == 4){
            str = "Phó phòng: ";
        }else{
            str = "Nhân viên: ";
        }
        str+= name + " (ID: "+id+")";
        return str;
    }
  
}
